﻿USE m3u3ej2;

/* EJERCICIO 1. Realizar un procedimiento almacenado que reciba un texto y un carácter. Debe indicarte si ese carácter está en el texto.
  Debéis realizarlo con: Locate, Position */

-- con locate
DELIMITER //
CREATE PROCEDURE ej1a(texto varchar(200), caracter char(1))
  COMMENT 'Procedimiento almacenado que reciba un texto y un carácter.
  Debe indicarte si ese carácter está en el texto.
  Utilizar LOCATE' -- el comment es una instruccion de CREATE que permite documentar una ayuda (queda almacenado)
  BEGIN
    DECLARE resultado varchar(30) DEFAULT 'El caracter no esta'
    IF LOCATE(caracter, texto)<>0 
      THEN 
      SET resultado='El caracter esta'
    END IF;
  END //
  DELIMITER ;

CALL ej1a ('casa', 'asa');
CALL ej1a ('casa', 'o');


DROP PROCEDURE IF EXISTS ej1b;
DELIMITER //
CREATE PROCEDURE ej1b(texto varchar(25), caracter char)
  BEGIN
IF (SELECT POSITION(caracter IN texto)) 
  THEN 
  SELECT 'Verdadero';
ELSE 
  SELECT 'Falso';
END IF;
  END //
  DELIMITER ;

CALL ej1b('casa', 'asa');
CALL ej1b ('casa', 'o');


/* EJERCICIO 2. Realizar un procedimiento almacenado que reciba un texto y un carácter. Te debe indicar 
  toodo el texto que haya antes de la primera vez que aparece ese carácter. */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej2(texto varchar(25), caracter char)
  BEGIN
SELECT SUBSTRING_INDEX(texto, caracter, 1);
  END //
  DELIMITER ;

CALL ej2 ('esternocleidomastoideo', 'i');

/* EJERCICIO 3. Realizar un procedimiento almacenado que reciba tres números y dos argumentos de tipo salida donde devuelva
  el número más grande y el número más pequeño de los tres números pasados. */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej3(num1 int, num2 int, num3 int, OUT mayor int, OUT menor int)
  COMMENT 'Realizar un procedimiento almacenado que reciba tres números
  y dos argumentos de tipo salida donde devuelva
  el número más grande y el número más pequeño de los tres números pasados'
  BEGIN
    SET mayor=GREATEST(num1, num2, num3);
    SET menor=LEAST(num1,num2,num3);

  END//
DELIMITER ;

CALL ej3(6,2,5,@4,@7);

/* EJERCICIO 4. Realizar un procedimiento almacenado que muestre cuantos numero1 y numeros 2 son mayores que 50 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4()
  COMMENT 'Realizar un procedimiento almacenado que muestre cuantos numero1 y numeros 2 son mayores que 50'
  BEGIN
    SELECT COUNT(numero1)n1 FROM datos_ejemplo2 WHERE numero1>50 
      UNION  
    SELECT COUNT(numero2)n2 FROM datos_ejemplo2 WHERE numero2 >50;

  END //
DELIMITER ;
CALL ej4();

SELECT * FROM datos_m3u3ej2;

/* EJERCICIO 5. Realizar un procedimiento almacenado que calcule la suma y la resta del numero1 y numero2*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej5()
  BEGIN
  UPDATE datos_ejemplo2
    SET suma=numero1+numero2;
  UPDATE datos_ejemplo2
    SET resta=numero1-numero2;  
  END //
DELIMITER ;
CALL ej5();
SELECT * FROM  datos_ejemplo2;

/* EJERCICIO 6. Realizar un procedimineto almacenado que primero ponga todos los valores de suma y resta a null y despues 
  la suma solamente si el numero 1 es mayor que el numero 2 y calcule la resta de numero2 menos numero 1
  si el numero 2 es mayor o numero 1 y numero 2 si es mayor que el numero 1 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej6()
  BEGIN
  UPDATE datos_ejemplo2 SET suma=NULL, resta=NULL;
  UPDATE datos_ejemplo2 SET suma=IF(numero1>numero2,numero1+numero2,NULL);
  /* con FROM
  UPDATE datos_ejemplo2 SET suma=numero1+numero2 WHERE numero1>numero2; */
  UPDATE datos_ejemplo2 SET resta=IF(numero1>numero2,numero1-numero2,numero2-numero1);
  END //
DELIMITER ;
CALL ej6();
SELECT * FROM  datos_ejemplo2;

/* EJERCICIO 7. Realizar un procedimiento almacenado que coloque en el campo junto el texto1,texto2 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej7 ()
  BEGIN
  UPDATE datos_ejemplo2 SET junto=CONCAT(texto1,' ',texto2);
  END //
DELIMITER;
CALL ej7();
SELECT * FROM datos_ejemplo2;


/* EJERCICIO 8. Realizar un procedimiento almacenado que coloque en el campo junto el valor NULL.
  Después debe colocar en el campo junto el texto1-texto2 si el rango es A y si es rango B debe colocar texto1+texto.
  Si el rango es distinto debe colocar texto1 nada más */
  DROP PROCEDURE IF EXISTS ej8;
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej8 ()
    BEGIN
      UPDATE datos_ejemplo2 SET junto=NULL;
      UPDATE datos_ejemplo2 SET junto=
        IF (rango='A', CONCAT(texto1,'-',texto2), IF(rango='B', CONCAT(texto1,'+',texto2), texto1));
    END //
  DELIMITER;
CALL ej8();
SELECT * FROM datos_ejemplo2;